(function () {
    angular.module('dint')
        .service('userSvr', ['$q', 'parseSvr', function ($q, parseSvr) {
            var findUsers = function (options) {
                var q = $q.defer();
                var User = parseSvr.Object.extend("User");
                var query = new parseSvr.Query(User);

                query.skip((options.pageNumber - 1) * options.pageSize).limit(options.pageSize);
                query.find({
                    success: function (results) {
                        q.resolve(results);

                    },
                    error: function (error) {
                        q.reject(error)
                        alert("Error: " + error.code + " " + error.message);
                    }
                });

                return q.promise;
            };
            var create = function (req) {
                console.log(req);
                var pUser = parseSvr.Object.extend("User");
                var user = new pUser();
                user.set('displayName', req.displayName);
                user.set('email', req.email);
                user.set('username', req.username);
                user.set('password', req.password);
                user.set('phone', req.phone);
                if (req.profilePictureSmall) {
                    var file = new Parse.File("logo", {base64: req.profilePictureSmall}, "image/jpeg");
                    file.save();
                    user.set('profilePictureSmall', file);
                }
                return user.save();
            };
            var deleteUser = function (id) {
                console.log(id);
                var User = Parse.Object.extend("User");
                var query = new Parse.Query(User);
                return query.get(id, {
                    success: function (nuser) {
                        nuser.destroy({useMasterKey: true});
                    },
                    error: function (object, error) {
                    }
                });
            };
            var putUser = function (req) {
                var pUser = parseSvr.Object.extend("User");
                var query = new Parse.Query(pUser);
                query.equalTo('objectId', req.id);
                var promise = query.first();
                promise = promise.then(function (user) {
                    user.set('displayName', req.displayName);
                    user.set('email', req.email);
                    user.set('username', req.username);
                    user.set('password', req.password);
                    user.set('phone', req.phone);
                    return user.save(null,{useMasterKey: true});
                }, function (error) {
                    return error;
                });
                return promise;
            };
            return {
                findUsers: findUsers,
                create: create,
                deleteUser: deleteUser,
                putUser: putUser
            }
        }])
})();