/**
 * Created by iAboShosha on 8/27/15.
 */
(function () {
    angular.module('dint')
        .service('dintSvr', ['$q', 'parseSvr', '$http', function ($q, parseSvr, $http) {
            var map = {
                text: 'Text',
                audio: 'Audio',
                doodle: 'Doodle',
                photo: 'Photo',
                video: 'Video'
            };
            var findUserDints = function (options) {
                var getContent = function (item) {

                    var type = item.get('type');
                    var table = map[type];

                    //var tObject = parseSvr.Object.extend(table);
                    //var tableQuery = new parseSvr.Query(tObject);

                    return get(table, item.id)
                        .then(function (data) {
                            item.content = data;
                            //return data;
                        });

                };
                var q = $q.defer();
                var Dint = parseSvr.Object.extend("Dint");
                var query = new parseSvr.Query(Dint);

                query.skip((options.pageNumber - 1) * options.pageSize).limit(options.pageSize);
                query.equalTo('user', options.userName);
                query.notEqualTo('deleted', true);
                query.find({
                    success: function (results) {
                        var qArr = [];

                        for (var i in results) {
                            qArr.push(getContent(results[i]))
                        }

                        $q.all(qArr).then(function () {
                            console.log('done------', results);
                            q.resolve(results);

                        });

                    },
                    error: function (error) {
                        q.reject(error)
                        alert("Error: " + error.code + " " + error.message);
                    }
                });

                return q.promise;
            };
            var get = function (table, dintId) {
                var q = $q.defer();
                if (!table) {
                    q.reject('no table !!')
                } else {

                    var Table = parseSvr.Object.extend(table);
                    var query = new parseSvr.Query(Table);

                    query.equalTo('dint', dintId);

                    query.find({
                        success: function (results) {
                            if (results.length >= 1)
                                q.resolve(results[0]);
                            else
                                q.resolve(null);

                        },
                        error: function (error) {
                            q.reject(error);
                            alert("Error: " + error.code + " " + error.message);
                        }
                    });
                }

                return q.promise;

            };

            return {
                findUserDints: findUserDints
            }
        }])
})();