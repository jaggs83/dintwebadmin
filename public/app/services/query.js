/**
 * Created by iAboShosha on 8/27/15.
 */
(function () {
    angular.module('dint')
        .service('dint', ['$q', 'parseSvr', '$http', function ($q, parseSvr, $http) {


            var map = {
                text: 'Text',
                audio: 'Audio',
                doodle: 'Doodle',
                photo: 'Photo',
                video: 'Video',


                textAdd: function (item) {
                    var Content = parseSvr.Object.extend(map[item.type]);
                    var content = new Content();
                    content.set('dint', item.id);
                    content.set('title', item.title);
                    content.set('textContent', item.textContent);
                    content.set('color', item.color);
                    return content.save()
                },
                audioAdd: function (item) {
                    var Content = parseSvr.Object.extend(map[item.type]);
                    var content = new Content();
                    content.set('dint', item.id);

                    content.set('title', item.title);
                    content.set('audio', new parseSvr.File('audio', item.audioFile));
                    content.set('audioUrl', item.audio);

                    //content.set('shortAudio', item.textContent);
                    //content.set('video', item.user);
                    return content.save()
                },
                doodleAdd: function (item) {
                    var Content = parseSvr.Object.extend(map[item.type]);
                    var content = new Content();
                    content.set('dint', item.id);
                    content.set('title', item.title);
                    content.set('doodle', new parseSvr.File('doodle', item.doodleFile));
                    content.set('doodleUrl', item.doodle);
                    content.set('thumbnail', new parseSvr.File('thumbnail', item.thumb));
                    content.set('thumbnailUrl', item.thumbnail);

                    return content.save()
                },
                photoAdd: function (item) {
                    var Content = parseSvr.Object.extend(map[item.type]);
                    var content = new Content();
                    content.set('dint', item.id);
                    content.set('title', item.title);
                    content.set('photo', new parseSvr.File('photo', item.photoFile));
                    content.set('photoUrl', item.photo);

                    content.set('thumbnail', new parseSvr.File('thumbnail', item.thumb));
                    content.set('thumbnailUrl', item.thumbnail);
                    return content.save()
                },
                videoAdd: function (item) {
                    var Content = parseSvr.Object.extend(map[item.type]);
                    var content = new Content();
                    content.set('dint', item.id);
                    content.set('title', item.title);

                    content.set('video', new parseSvr.File('video', item.videoFile));
                    content.set('videoUrl', item.video);

                    content.set('thumbnail', new parseSvr.File('thumbnail', item.thumb));
                    content.set('thumbnailUrl', item.thumbnail);
                    return content.save()
                },
            };
            var find = function (options) {


                var getContent = function (item) {

                    var type = item.get('type');
                    var table = map[type];

                    //var tObject = parseSvr.Object.extend(table);
                    //var tableQuery = new parseSvr.Query(tObject);

                    return get(table, item.id)
                        .then(function (data) {
                            item.content = data;
                            //return data;
                        });

                };
                var q = $q.defer();
                var Dint = parseSvr.Object.extend("Dint");
                var query = new parseSvr.Query(Dint);

                query.skip((options.pageNumber - 1) * options.pageSize).limit(options.pageSize);
                query.notEqualTo('deleted', true);
                query.find({
                    success: function (results) {
                        var qArr = [];

                        for (var i in results) {
                            qArr.push(getContent(results[i]))
                        }

                        $q.all(qArr).then(function () {
                            console.log('done------', results);
                            q.resolve(results);

                        });

                    },
                    error: function (error) {
                        q.reject(error)
                        alert("Error: " + error.code + " " + error.message);
                    }
                });

                return q.promise;
            };

            var get = function (table, dintId) {
                var q = $q.defer();
                if (!table) {
                    q.reject('no table !!')
                } else {

                    var Table = parseSvr.Object.extend(table);
                    var query = new parseSvr.Query(Table);

                    query.equalTo('dint', dintId);

                    query.find({
                        success: function (results) {
                            if (results.length >= 1)
                                q.resolve(results[0]);
                            else
                                q.resolve(null);

                        },
                        error: function (error) {
                            q.reject(error);
                            alert("Error: " + error.code + " " + error.message);
                        }
                    });
                }

                return q.promise;

            };


            var _import = function (item) {
                var Dint = parseSvr.Object.extend('Dint');
                var dint = new Dint();
                var location = item.location.split(',');
                location[0] = location[0] * 1.0;
                location[1] = location[1] * 1.0;
                var point = new Parse.GeoPoint({latitude: location[0], longitude: location[1]});
                dint.set('user', item.user);
                dint.set('access', item.access);
                dint.set('tags', item.tags);
                dint.set('type', item.type);
                dint.set('title', item.title);
                dint.set('location', point);
                dint.set('datePosted', new Date());
                dint.set('imported', true);


                ////content
                //var Content = parseSvr.Object.extend(map[item.type]);
                //var content = new Content();

                return dint.save(null, {
                    success: function (o) {

                        var func = map[item.type + "Add"];
                        if (func) {
                            var h = func(item);
                            return h;
                        }

                    },
                    error: function (o, error) {
                        console.log(o, error);
                    }
                })
            };

            return {
                find: find,
                get: get,
                import: _import
            }
        }])
})();