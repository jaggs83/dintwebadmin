/**
 * Created by iAboShosha on 8/27/15.
 */
(function () {
    angular.module('dint')
        .service('groupSvr', ['$q', 'parseSvr', '$http', function ($q, parseSvr, $http) {
            var findUserGroups = function (options) {
                var q = $q.defer();
                var Groups = parseSvr.Object.extend("Groups");
                var query = new parseSvr.Query(Groups);
                query.equalTo('user', options.userName);
                query.skip((options.pageNumber - 1) * options.pageSize).limit(options.pageSize);
                query.find({
                    success: function (results) {
                        q.resolve(results);

                    },
                    error: function (error) {
                        q.reject(error)
                        alert("Error: " + error.code + " " + error.message);
                    }
                });

                return q.promise;
            };
            return {
                findUserGroups: findUserGroups
            }
        }])
})();