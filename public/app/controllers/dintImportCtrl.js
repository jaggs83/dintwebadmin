/**
 * Created by iAboShosha on 8/27/15.
 */
(function () {
    angular.module('dint')
        .controller('DintImportCtrl', ['$scope', 'nav', '$q', 'dint', function ($scope, nav, $q, query) {

            $scope.importedData = [];
            var read = function () {
                var defer = $q.defer();
                var fileUpload = document.getElementById("dintUpload");
                if (fileUpload.files && fileUpload.files.length) {

                    var reader = new FileReader();
                    reader.addEventListener('load', function () {
                        var file = reader.result;
                        Papa.parse(file, {
                            header: true,
                            complete: function (results) {
                                console.log("Finished:", results.data);
                                defer.resolve(results.data);
                            }
                        });

                    });
                    reader.readAsText(fileUpload.files[0], 'utf-8');

                } else {
                    defer.resolve(null)
                }
                return defer.promise;
            };


            $scope.start = function () {
                return read().then(function (m) {
                    $scope.importedData = m;
                    console.log(m);

                })
            };

            $scope.import = function () {
                var requests = [];



                for (var i in $scope.importedData) {
                    var o = $scope.importedData[i];



                    requests.push(query.import(o)
                        .then(function () {
                            o.imported = true
                        }, function () {
                            o.imported = false
                        }));
                }
                $q.all(requests).then(function () {
                    $scope.importedData = [];

                })


            };

        }]);
})();

//var lineSeparator = '\n';
//if (file.indexOf('\r\n') > -1) {
//    lineSeparator = '\r\n';
//}
//else if (file.indexOf('\r') > -1) {
//    lineSeparator = '\r';
//}
//var lines = file.split(lineSeparator);
////var head = lines.shift().split(';');
//var arr = [];
//
//
//for (var i = 0; lines && i < lines.length; i++) {
//    if (lines[i] && lines[i].length > 0) {
//        var data = lines[i].split(';');
//
//        if (data) {
//            arr.push({
//                type: data[0],
//                title: data[1],
//                tags: data[2],
//                datePosted: data[3],
//                access: data[4],
//                location: data[5],
//                user: data[6],
//                audio: data[7],
//                video: data[8],
//                doodle: data[9],
//                photo: data[10],
//                thumbnail: data[11],
//                textContent: data[12],
//                color: data[13]
//            });
//        }
//    }
//}

//var allTextLines = file.split(/\r\n|\n/);
//var headers = allTextLines[0].split(',');
//var lines = [];
//
//for (var i=1; i<allTextLines.length; i++) {
//    var data = allTextLines[i].split(',');
//    if (data.length == headers.length) {
//
//        var tarr = [];
//        for (var j=0; j<headers.length; j++) {
//            tarr.push(headers[j]+":"+data[j]);
//        }
//        lines.push(tarr);
//    }
//}
//
//defer.resolve(lines);