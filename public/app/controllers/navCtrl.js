/**
 * Created by iAboShosha on 8/27/15.
 */
(function () {
    angular.module('dint')
        .controller('NavCtrl', ['$scope','nav',function ($scope,nav) {
            $scope.nav=nav;
        }]);
})();