/**
 * Created by iAboShosha on 8/27/15.
 */
(function () {
    angular.module('dint')
        .controller('UserCtrl', ['$scope', 'nav', 'userSvr', 'dintSvr', 'groupSvr', 'notify', function ($scope, nav, query, dintSvr, groupSvr, notify) {
            nav.title = "Accounts";
            nav.desc = "User list";
            nav.brad = ["users"];
            var columnDefs = [
                {width: 90, minWidth: 100, headerName: "*", field: "id", templateUrl: "templates/grid/operations.html"},
                {
                    width: 90, minWidth: 100, headerName: "Profile", field: "picture", cellRenderer: function (params) {
                    var resultElement = $("<img />");
                    if (params.value)
                        $(resultElement).attr('src', params.value._url);
                    $(resultElement).attr('width', "30px");
                    return resultElement[0];
                }
                },
                {width: 150, minWidth: 100, headerName: "Name", field: "displayName"},
                {width: 150, minWidth: 100, headerName: "Email", field: "email"},
                {width: 150, minWidth: 100, headerName: "Phone", field: "phone"},
                {width: 150, minWidth: 100, headerName: "About", field: "about"}
            ];


            $scope.gridOptions = {
                angularCompileRows: true,
                rowHeight: '30',
                columnDefs: columnDefs
            };

            var pSize = 50;

            var loadUsers = function () {
                $scope.gridOptions.api.setDatasource(dataSource);
            };
            var dataSource = {
                pageSize: pSize,
                getRows: function (params) {
                    query.findUsers({pageNumber: params.endRow / pSize, pageSize: pSize})
                        .then(function (data) {
                            console.log(data);
                            var arr = [];
                            for (var o in data) {
                                arr.push({
                                    id: data[o].id,
                                    displayName: data[o].get('displayName'),
                                    email: data[o].get('email'),
                                    username: data[o].get('username'),
                                    password: data[o].get('password'),
                                    about: data[o].get('about'),
                                    phone: data[o].get('phone'),
                                    picture: data[o].get('profilePictureSmall')

                                })
                            }
                            console.log(arr);
                            params.successCallback(arr, -1);
                            $scope.data = arr;
                            $scope.org = data;
                        });

                }
            };
            setTimeout(function () {
                loadUsers();
            }, 500);
            $scope.createUser = function () {
                $scope.validForm = {};
                $scope.User = {};
                $('#save').show();
                $('#update').hide();
                $('#userEdit').modal('show');
            };

            $scope.saveUser = function (user) {
                console.log("ssss", $scope.validForm, user);
                $scope.validForm.$submitted = true;
                /*if (!$scope.validForm.$valid) {
                 notify.Clear();
                 notify.Error("Form Invalid");
                 } else {*/
                notify.Info("Loading");
                query.create(user).then(function (data, status) {
                    $('#userEdit').modal('hide');
                    notify.Clear();
                    notify.Success("User Created");
                    loadUsers();
                }, function (data) {
                    notify.Clear();
                    notify.Error(data.message);
                });
                //}
            };
            $scope.deleteUser = function (user) {
                var result = confirm("Are you sure you want to delete this user");
                if (result == true)
                    query.deleteUser(user.id).then(function (data, status) {
                        notify.Clear();
                        notify.Success("User Deleted");
                        loadUsers();
                    }, function (data) {
                        notify.Clear();
                        notify.Error(data.message);
                    });

            };
            var dintcolumnDefs = [
                {width: 200, minWidth: 100, headerName: "Title", field: "title"},
                {width: 200, minWidth: 100, headerName: "Tags", field: "tags"},
                {
                    width: 200,
                    minWidth: 200,
                    headerName: "content",
                    field: "content",
                    suppressSizeToFit: true,
                    templateUrl: "templates/grid/content.html"
                }
            ];


            $scope.dintgridOptions = {
                angularCompileRows: true,
                rowHeight: '90',
                columnDefs: dintcolumnDefs
            };


            $scope.userDints = function (user) {
                var dintDataSource = {
                    pageSize: pSize,
                    getRows: function (params) {
                        console.log(params, 'params');
                        dintSvr.findUserDints({
                            pageNumber: params.endRow / pSize,
                            pageSize: pSize,
                            userName: user.username
                        })
                            .then(function (data) {

                                var arr = [];
                                for (var o in data) {
                                    arr.push({
                                        id: data[o].id,
                                        user: data[o].get('user'),
                                        type: data[o].get('type'),
                                        createdAt: data[o].get('createdAt'),
                                        updatedAt: data[o].get('updatedAt'),
                                        location: data[o].get('location'),
                                        title: data[o].get('title'),
                                        flag: data[o].get('flag'),
                                        content: data[o].content

                                    })
                                }
                                params.successCallback(arr, -1);
                                $scope.data = arr;
                                $scope.org = data;
                            });

                    }
                };
                $scope.dintgridOptions.api.setDatasource(dintDataSource);
                $('#userDintList').modal('show');

            };

            var groupcolumnDefs = [
                {width: 200, minWidth: 100, headerName: "Group Name", field: "groupname"},
                {width: 200, minWidth: 100, headerName: "Description", field: "description"}
            ];


            $scope.groupgridOptions = {
                angularCompileRows: true,
                rowHeight: '30',
                columnDefs: groupcolumnDefs
            };

            $scope.userGroups = function (user) {
                var groupDataSource = {
                    pageSize: pSize,
                    getRows: function (params) {
                        console.log(params, 'params');
                        groupSvr.findUserGroups({
                            pageNumber: params.endRow / pSize,
                            pageSize: pSize,
                            userName: user.username
                        })
                            .then(function (data) {

                                var arr = [];
                                for (var o in data) {
                                    arr.push({
                                        id: data[o].id,
                                        groupname: data[o].get('groupname'),
                                        description: data[o].get('description')

                                    })
                                }
                                params.successCallback(arr, -1);
                                $scope.data = arr;
                                $scope.org = data;
                            });

                    }
                };
                $scope.groupgridOptions.api.setDatasource(groupDataSource);
                $('#userGroupList').modal('show');

            };

            $scope.User = {};
            $scope.editUser = function (user) {
                $scope.validForm = {};
                $('#update').show();
                $('#save').hide();
                $scope.User = user;
                console.log($scope.User);
                $('#userEdit').modal('show');
            };
            $scope.updateUser = function (user) {
                console.log("ssss", $scope.validForm, user);
                $scope.validForm.$submitted = true;
                notify.Info("Loading");
                query.putUser(user).then(function (data, status) {
                    $('#userEdit').modal('hide');
                    notify.Clear();
                    notify.Success("User Updated");
                    loadUsers();
                }, function (data) {
                    notify.Clear();
                    notify.Error(data.message);
                });
            };
        }])
})();