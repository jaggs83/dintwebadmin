/**
 * Created by iAboShosha on 8/27/15.
 */
(function () {
    angular.module('dint')
        .controller('DintCtrl', ['$scope', 'nav', 'dint','notify', function ($scope, nav, query,notify) {
            nav.title = "Dint";
            nav.desc = " list";
            nav.brad = ["Dint"];


            var columnDefs = [
                {width:90,minWidth: 100, headerName: "*", field: "flag", templateUrl: "templates/grid/flag.html"},
                {width:150,minWidth: 100, headerName: "User", field: "user"},
                {width:200,minWidth: 100, headerName: "Title", field: "title"},
                {width:1000,minWidth: 500, headerName: "content", field: "content",suppressSizeToFit:true, templateUrl: "templates/grid/content.html"}
            ];


            $scope.gridOptions = {
                angularCompileRows: true,
                rowHeight: '90',
                columnDefs: columnDefs
            };

            var pSize = 5;
            var dataSource = {
                pageSize: pSize,
                getRows: function (params) {
                    console.log(params, 'params');
                    query.find({pageNumber: params.endRow / pSize, pageSize: pSize})
                        .then(function (data) {

                            var arr = [];
                            for (var o in data) {
                                arr.push({
                                    id: data[o].id,
                                    user: data[o].get('user'),
                                    type: data[o].get('type'),
                                    createdAt: data[o].get('createdAt'),
                                    updatedAt: data[o].get('updatedAt'),
                                    location: data[o].get('location'),
                                    title: data[o].get('title'),
                                    flag: data[o].get('flag'),
                                    content: data[o].content

                                })
                            }
                            params.successCallback(arr, -1);
                            $scope.data = arr;
                            $scope.org = data;
                        });

                }
            };

            setTimeout(function () {
                $scope.gridOptions.api.setDatasource(dataSource);
            }, 500);


            $scope.toogleFlag = function (data) {
                var o = _.chain($scope.org)
                    .find(function (i) {
                        return data.id == i.id
                    }).value();

                data.flag = data.flag == undefined ? false : data.flag;

                o.save('flag', !data.flag)
                    .then(function () {
                        console.log(data);
                        data.flag = !data.flag;
                        notify.Success('Successfully updated ');
                    },function(e){
                        notify.Error(e.message);
                        console.log(arguments,'delete error');
                    });

            };
            $scope.deleteDint = function (data) {
                var o = _.chain($scope.org)
                    .find(function (i) {
                        return data.id == i.id
                    }).value();


                //if(window.confirm('are you sure ?')) {
                    o.save('deleted', true)
                        .then(function () {
                            notify.Success('Successfully updated ');
                            var m = _.chain($scope.data)
                                .find(function (i) {
                                    return data.id == i.id
                                }).value();



                            $scope.data.splice($scope.data.indexOf(m), 1);
                            $scope.gridOptions.api.setDatasource(dataSource);

                        },function(e){
                            notify.Error(e.message);
                            console.log(arguments,'delete error');
                        });
                //}
            }
        }]);
})();
