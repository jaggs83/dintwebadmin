/**
 * Created by iAboShosha on 8/25/15.
 */

(function () {
    angular.module('dint', ["angularGrid", "ngRoute"])
        .config(['$routeProvider',
            function ($routeProvider) {
                $routeProvider.
                    when('/user-list', {
                        templateUrl: 'templates/user-list.html',
                        controller: 'UserCtrl'
                    }).
                    when('/dint-list', {
                        templateUrl: 'templates/dint-list.html',
                        controller: 'DintCtrl'
                    }).
                    when('/dint-import', {
                        templateUrl: 'templates/dint-import.html',
                        controller: 'DintImportCtrl'
                    }).
                    when('/login',{
                        templateUrl: 'templates/login.html',
                        controller: 'LoginCtrl'
                    }).
                    otherwise({
                        redirectTo: '/user-list'
                    });
            }])
        .run(['$rootScope', '$location','parseSvr',function($rootScope, $location,parseSvr){

            $rootScope.$on('$routeChangeStart', function (event, next, current) {
                // if route requires auth and user is not logged in
                console.log(parseSvr.User.current());
                if (!parseSvr.User.current()) {
                    // redirect back to login
                    $location.path('/login');
                }
            });

        }])

        .directive('viewTemplate', function () {

            return {
                restrict: 'AE',

                scope: {
                    content: '=',
                    templateType: '='

                },
                templateUrl: function (elem, attr) {
                    console.log();
                    return "templates/" + attr.templateType + ".html"
                }

            }
        })


    //.controller('grid', ['$scope', 'query', function ($scope, query) {
    //
    //    var load = function () {
    //        return query.find($scope.paging)
    //            .then(function (data) {
    //                $scope.model = data;
    //
    //                for (var i in data) {
    //                    $scope.getContent(data[i])
    //                }
    //            });
    //    };
    //
    //
    //    var map = {
    //        text: 'Text',
    //        audio: 'Audio',
    //        doodle: 'Doodle',
    //        photo: 'Photo',
    //        video: 'Video',
    //    };
    //    $scope.getContent = function (item) {
    //
    //        var type = item.get('type');
    //        var table = map[type];
    //        return query.get(table, item.id)
    //            .then(function (data) {
    //                item.content = data;
    //            })
    //
    //    };
    //    $scope.paging = {pageNumber: 1, pageSize: 50};
    //    $scope.nextPage = function () {
    //        //need to get the total
    //        if ($scope.paging.pageSize < $scope.model.length) {
    //            return;
    //        }
    //        $scope.paging.pageNumber++;
    //        load();
    //    };
    //    $scope.prePage = function () {
    //        if ($scope.paging.pageNumber == 1) {
    //            return;
    //        }
    //        $scope.paging.pageNumber--;
    //        load();
    //    };
    //
    //
    //    load();
    //}])


})();
