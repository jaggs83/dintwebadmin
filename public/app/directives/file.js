/**
 * Created by iAboShosha on 9/3/15.
 */
(function () {
    angular.module('dint')
        .directive('file', function () {
            return {
                restrict: 'E',
                template: '<input class="form-control col-md-2" type="file" />',
                replace: true,
                require: 'ngModel',
                link: function (scope, element, attr, ctrl) {
                    var listener = function () {
                        scope.$apply(function () {
                            attr.multiple ? ctrl.$setViewValue(element[0].files) : ctrl.$setViewValue(element[0].files[0]);
                        });
                    }
                    element.bind('change', listener);
                }
            }
        });

})();